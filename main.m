clear;
clc;
format long;

%choose one Nerlist file to run
disp( '***Please choose one Netlist!***' );
disp( '1 ---> pulse_rlc_s3.sp' );
disp( '2 ---> sin_rlc_s3.sp' );
disp( '3 ---> bus8bit8seg.sp' );
disp( '4 ---> bus32seg16.sp' );
test_file = input( '\n' );

%load matrixs from stamp
switch(test_file)
    case 1
        load 'data_sample_pulse.mat';
        filename = 'pulse_rlc_s3';
        result_filename = 'result_pulse_rlc_s3';
    case 2
        load 'data_sample_sin.mat';
        filename = 'sin_rlc_s3';
        result_filename = 'result_sin_rlc_s3';
    case 3
        load 'data_sample_bus8.mat';
        filename = 'bus8bit8seg';
        result_filename = 'result_bus8bit8seg';
    case 4
        load 'data_sample_bus32.mat';
        filename = 'bus32seg16';
        result_filename = 'result_bus32seg16';
    otherwise
        error('Wrong input argument');
end

%T_start: the start of simulate time
%T_end: the end of simulate time
%N: the numbers of iteration
%error_limited: the limited error to stop iteration
if test_file == 1
    T_start = 0;
    T_end = 0.02;
    N = 20000;
    error_limited = 1e-9;
elseif test_file == 2
    T_start = 0;
    T_end = 0.02;
    N = 20000;
    error_limited = 1e-9;
elseif test_file == 3
    T_start = 0;
    T_end = 1e-9;
    N = 500;
    error_limited = 1e-9;
elseif test_file == 4
    T_start = 0;
    T_end = 1e-9;
    N = 500;
    error_limited = 1e-9;
end
    
%get the .lis file information
lis_file = strcat(filename,'.lis');
hspice_data = read_data(lis_file);

%choose one method to run
disp( '***Please choose one method!***' );
disp( '1 ---> Back_Euler' );
disp( '2 ---> Trapezoid' );
choice_method = input( '\n' );

%begin to iterate
tic;
switch (choice_method)
    case 1
    %get the result calculated by Back Euler method
    [results, src_value] = Back_Euler(C, G, B, LT, SRC, T_start, T_end, N, error_limited);
    case 2
    %get the result calculated by Trapezoid method
    [results, src_value] = Trapezoid(C, G, B, LT, SRC, T_start, T_end, N, error_limited);
    otherwise
    error('Wrong input argument')
end

toc;

%compute the error
[error , max_error ,MSE] = calc_error(hspice_data,results);

%store data
result_file = strcat(result_filename,'.mat');
save( result_file, 'hspice_data','results','src_value','N','error','max_error','MSE','-mat');

%display the figures and print results
switch (choice_method)
    case 1
		show_dist_Back_Euler( result_file );
    case 2
		show_dist_Trapezoid( result_file );
    otherwise

end


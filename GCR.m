function re = GCR(M, b, x_iteration, error_limited, step_limited)

% Approximately Solving: 
%                      Mx = b
% x_iteration: input is the initial value of x, start the iteration; 
%              then store the iterative results of x;
% error_limited: the limited error to stop iteration
% step_limited: the maximum steps of GCR to stop iteration
%
% re: return the result of x

step = 0;
error = inf;
k = 1;
N = size(M,1); % dimension of x

% test if the b vector nonzero
if norm(b) == 0
    re = zeros(N,1);
    return;
end

r_iteration = b - M * x_iteration; % r0 = b - M * x0
p = zeros(N, step_limited);
MP_interation = zeros(N, step_limited);

% start interation
p(:,1) = r_iteration;
while error >= error_limited && step < step_limited
    MP_interation(:,k) = M * p(:,k);
    alpha_interation = ((r_iteration)' * MP_interation(:,k)) / (MP_interation(:,k)' * MP_interation(:,k));
    x_iteration = x_iteration + alpha_interation .* p(:,k); % x(k+1) = x(k) + a(k)p(k) 
    r_iteration = r_iteration - alpha_interation .* MP_interation(:,k); % r(k+1) = r(k) - a(k)Mp(k)
    p(:,k+1) = r_iteration;
    for j = 1:1:k
        beta_interation = ((M * r_iteration)' * MP_interation(:,j)) / (MP_interation(:,j)' * MP_interation(:,j));
        p(:,k+1) = p(:,k+1) - beta_interation .* p(:,j);
    end
    k = k + 1;
    step = step + 1;
    error = norm(r_iteration,2) / norm(b,2);
end

% return the approximate solution of x
re = x_iteration;

function show_dist_Trapezoid( filename )
load(filename,'-mat');
fprintf('the iteration number of Trapezoid is:\n');
disp(N);
fprintf('the maximum error (absolute) is:\n');
disp(max_error);
fprintf('the mean square error is:\n');
disp(MSE);

subplot(2,2,1);
plot(results(:,1),src_value(:,:),'b');
title('Fig.1: input signal waveform','FontSize',14);
xlabel('t(s)','FontSize',14);
ylabel('Voltage(V)','FontSize',14);

subplot(2,2,2);
plot(results(:,1),results(:,2),'r');
title('Fig.2: output of Trapezoid simuation','FontSize',14);
xlabel('t(s)','FontSize',14);
ylabel('Voltage(V)','FontSize',14);

subplot(2,2,3);
plot(results(:,1),results(:,2),'b--');
hold on;
plot(hspice_data(:,1),hspice_data(:,2),'r');
title('Fig.3: comparison between simulation and spice result','FontSize',14);
xlabel('t(s)','FontSize',14);
ylabel('Voltage(V)','FontSize',14);
legend('Trapezoid simuation','hspice result');

subplot(2,2,4);
plot(error(:,1),error(:,2),'c');
title('Fig.4: absolute error distribution','FontSize',14);
xlabel('t(s)','FontSize',14);
ylabel('Voltage(V)','FontSize',14);